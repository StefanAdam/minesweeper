#ifndef VECTOR_H
#define VECTOR_H
#include <vector>
#endif


using namespace std;


class minesweeperBoard {

public:

	char Board[][MAXSIDE];
	// 0 = Empty
	// 1 = Mine

	int mines[MAXMINES][2];
	// MAXSIDE
	// SIDE
	// MINES
	// MAXMINES
	
	void reset() 
	{
    	// Initiate the random number generator so that
    	// the same configuration doesn't arises
		srand(time(NULL));

		// Assign all cells as mine-false
		for (int i=0; i<SIDE; i++)
		{
			for (int j=0; j<SIDE; j++)
			{
				this->Board[i][j] = '-';
			}
		}

		return;
	}




	void placeMines(int mines[][2])
	{
		bool mark[MAXSIDE*MAXSIDE];

		memset (mark, false, sizeof (mark));

		//contunue until all random mines have been created

		for (int i=0; i<MINES; )
		{
			int random = rand() % (SIDE*SIDE);
			int x = random / SIDE;
			int y = random % SIDE;

			// Add the mine if no mine is placed at this position
			if (mark[random] == false)
			{
				// Row Index of Mine
				mines[i][0] = x;
				// Column Index of Mine
				mines[i][1] = y;
				
				// Place the Mine
				this->Board[mines[i][0]][mines[i][1]] = '*';
				mark[random] = true;
				i++;
			}
		}
		return;
	}


	void printBoard()
	{
	    int i, j;
	 
	    printf ("    ");
	 
	    for (i=0; i<SIDE; i++)
	        printf ("%d ", i);
	 
	    printf ("\n\n");
	 
	    for (i=0; i<SIDE; i++)
	    {
	        printf ("%d   ", i);
	 
	        for (j=0; j<SIDE; j++)
	            printf ("%c ", this->Board[i][j]);
	        printf ("\n");
	    }
	    return;
	}

	void revealMine(minesweeperBoard mineBoardObj)
	{
		// Reveal all mines from mineBoardObj.Board on playerBoardObj.Board.
			// Dump all mines from mineBoardObj.Board into playerBoardObj.Board
				// Check every tile in mineBoardObj.Board = '*'
					// If tile is '*' in mineBoardObj.Board set tile to '*' in PlayerBoardObj.Board
			for (int i = 0; i < SIDE; ++i)
			{
				for (int j = 0; j < SIDE; ++j)
				{
					if (mineBoardObj.Board[i][j] == '*')
					{
						this->Board[i][j] = '*';
					}
				}
			}
			return;
	}

	void cheatMinesweeper()
	{
	    for (int i = 0; i < SIDE; ++i)
	   	{
	    	for (int j = 0; j < SIDE; ++j)
	    	{
	            std::cout << this->Board[i][j] << ' ';
	        }
	        std::cout << std::endl;
		}
    }



};
