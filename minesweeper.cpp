// A C++ Program to Implement and Play Minesweeper
 
#include<bits/stdc++.h>



using namespace std;
 
#define BEGINNER 0
#define INTERMEDIATE 1
#define ADVANCED 2
#define MAXSIDE 25
#define MAXMINES 99
#define MOVESIZE 526 // (25 * 25 - 99)

int SIDE ; // side length of the board
int MINES ; // number of mines on the board

#ifndef DIFFICULTY_H
#define DIFFICULTY_H

#include "difficulty.h"
#endif

#ifndef BOARD_H
#define BOARD_H

#include "board.h"
#endif

// Actual Board and My Board
minesweeperBoard mineBoardObj;
minesweeperBoard playerBoardObj;
 

 
// A Utility Function to check whether given cell (row, col)
// is a valid cell or not
bool isValid(int row, int col)
{
    // Returns true if row number and column number
    // is in range
    if (row >= 0) 
    {
        if (row < SIDE)
        {
            if (col >= 0) 
            {
                if (col < SIDE) 
                {
                    //cout << row << ' ' << col << endl;
                    //printf("IsValid\n");

                    return true;
                }
            }
        }
    }
    //cout << row << ' ' << col << endl;
    //printf("IsNotValid\n");
    return false;

}
 
// A Utility Function to check whether given cell (row, col)
// has a mine or not.
bool isMine (int row, int col, char board[][MAXSIDE])
{
    if (board[row][col] == '*')
    {
        //printf("isMine\n");
        return (true);  
    }
        
    else
    {
        //printf("isNotMine\n");
        return (false);
    }
        
}
 
// A Function to get the user's move
void makeMove(int *x, int *y)
{
    // Take the input move
    printf("Enter your move, (row, column) -> ");
    scanf("%d %d", x, y);
    return;
}
 

int countAdjacentMines(int row, int col, int mines[][2], char mineBoard[][MAXSIDE])
{
 
    int i;
    int count = 0;
 
    /*
        Count all the mines in the 8 adjacent
        cells
 
            N.W   N   N.E
              \   |   /
               \  |  /
            W----Cell----E
                 / | \
               /   |  \
            S.W    S   S.E
 
        Cell-->Current Cell (row, col)
        N -->  North        (row-1, col)
        S -->  South        (row+1, col)
        E -->  East         (row, col+1)
        W -->  West            (row, col-1)
        N.E--> North-East   (row-1, col+1)
        N.W--> North-West   (row-1, col-1)
        S.E--> South-East   (row+1, col+1)
        S.W--> South-West   (row+1, col-1)
    */
 
    //----------- 1st Neighbour (North) ------------
 
        // Only process this cell if this is a valid one
        if (isValid (row-1, col) == true)
        {
               if (isMine (row-1, col, mineBoard) == true)
                {
                    count++;
                    //printf("NorthMine\n");
                }
        }
 
    //----------- 2nd Neighbour (South) ------------
 
        // Only process this cell if this is a valid one
        if (isValid (row+1, col) == true)
        {
               if (isMine (row+1, col, mineBoard) == true)
                {
                    count++;
                    //printf("SouthMine\n");
                }
        }
 
    //----------- 3rd Neighbour (East) ------------
 
        // Only process this cell if this is a valid one
        if (isValid (row, col+1) == true)
        {
            if (isMine (row, col+1, mineBoard) == true)
                {
                    count++;
                    //printf("EastMine\n");
                }
        }
 
    //----------- 4th Neighbour (West) ------------
 
        // Only process this cell if this is a valid one
        if (isValid (row, col-1) == true)
        {
               if (isMine (row, col-1, mineBoard) == true)
                {
                    count++;
                    //printf("WestMine\n");
                }
        }
 
    //----------- 5th Neighbour (North-East) ------------
 
        // Only process this cell if this is a valid one
        if (isValid (row-1, col+1) == true)
        {
            if (isMine (row-1, col+1, mineBoard) == true)
                {
                    count++;
                    //printf("North-EastMine\n");
                }
        }
 
     //----------- 6th Neighbour (North-West) ------------
 
        // Only process this cell if this is a valid one
        if (isValid (row-1, col-1) == true)
        {
             if (isMine (row-1, col-1, mineBoard) == true)
                {
                    count++;
                    //printf("North-WestMine\n");
                }
        }
 
     //----------- 7th Neighbour (South-East) ------------
 
        // Only process this cell if this is a valid one
        if (isValid (row+1, col+1) == true)
        {
               if (isMine (row+1, col+1, mineBoard) == true)
                {
                    count++;
                    //printf("South-EastMine\n");
                }
        }
 
    //----------- 8th Neighbour (South-West) ------------
 
        // Only process this cell if this is a valid one
        if (isValid (row+1, col-1) == true)
        {
            if (isMine (row+1, col-1, mineBoard) == true)
                {
                    count++;
                    //printf("South-WestMine\n");
                }
        }
 
    return (count);
}

// A function to replace the mine from (row, col) and put
// it to a vacant space
void replaceMine (int row, int col, char board[][MAXSIDE])
{
    for (int i=0; i<SIDE; i++)
    {
        for (int j=0; j<SIDE; j++)
            {
                // Find the first location in the board
                // which is not having a mine and put a mine
                // there.
                if (board[i][j] != '*')
                {
                    board[i][j] = '*';
                    board[row][col] = '-';
                    return;
                }
            }
    }
    return;
}



// A Recursive Fucntion to play the Minesweeper Game
bool playMinesweeperUtil(char playerBoard[][MAXSIDE], char mineBoard[][MAXSIDE], int mines[][2], int row, int col, int *movesLeft)
{

    // Base Case of Recursion
    if (playerBoard[row][col] != '-')
        return (false);

    int i, j;

    // You opened a mine
    // You are going to lose
    if (mineBoard[row][col] == '*')
    {
        playerBoard[row][col] = '*';

        playerBoardObj.revealMine(mineBoardObj);

        playerBoardObj.printBoard();
        printf ("\nYou Lost!\n");
        return (true) ;
    }

 
    else
     {
        // Calculate the number of adjacent mines and put it
        // on the board
        int count = countAdjacentMines(row, col, mines, mineBoard);
        (*movesLeft)--;
 
        playerBoardObj.Board[row][col] = count + '0';
 
        if (count == 0)
        {
            /*
            Recur for all 8 adjacent cells
 
                N.W   N   N.E
                  \   |   /
                      \  |  /
                W----Cell----E
                     / | \
                   /   |  \
                S.W    S   S.E
 
            Cell-->Current Cell (row, col)
            N -->  North        (row-1, col)
            S -->  South        (row+1, col)
            E -->  East         (row, col+1)
            W -->  West            (row, col-1)
            N.E--> North-East   (row-1, col+1)
            N.W--> North-West   (row-1, col-1)
            S.E--> South-East   (row+1, col+1)
            S.W--> South-West   (row+1, col-1)
            */
 
                //----------- 1st Neighbour (North) ------------
 
            // Only process this cell if this is a valid one
            if (isValid (row-1, col) == true)
            {
                   if (isMine (row-1, col, mineBoardObj.Board) == false)
                   playMinesweeperUtil(playerBoardObj.Board, mineBoardObj.Board, mineBoardObj.mines, row-1, col, movesLeft);
            }
 
            //----------- 2nd Neighbour (South) ------------
 
            // Only process this cell if this is a valid one
            if (isValid (row+1, col) == true)
            {
                   if (isMine (row+1, col, mineBoardObj.Board) == false)
                    playMinesweeperUtil(playerBoardObj.Board, mineBoardObj.Board, mineBoardObj.mines, row+1, col, movesLeft);
            }
 
            //----------- 3rd Neighbour (East) ------------
 
            // Only process this cell if this is a valid one
            if (isValid (row, col+1) == true)
            {
                if (isMine (row, col+1, mineBoardObj.Board) == false)
                    playMinesweeperUtil(playerBoardObj.Board, mineBoardObj.Board, mineBoardObj.mines, row, col+1, movesLeft);
            }
 
            //----------- 4th Neighbour (West) ------------
 
            // Only process this cell if this is a valid one
            if (isValid (row, col-1) == true)
            {
                   if (isMine (row, col-1, mineBoardObj.Board) == false)
                    playMinesweeperUtil(playerBoardObj.Board, mineBoardObj.Board, mineBoardObj.mines, row, col-1, movesLeft);
            }
 
            //----------- 5th Neighbour (North-East) ------------
 
            // Only process this cell if this is a valid one
            if (isValid (row-1, col+1) == true)
            {
                if (isMine (row-1, col+1, mineBoardObj.Board) == false)
                    playMinesweeperUtil(playerBoardObj.Board, mineBoardObj.Board, mineBoardObj.mines, row-1, col+1, movesLeft);
            }
 
             //----------- 6th Neighbour (North-West) ------------
 
            // Only process this cell if this is a valid one
            if (isValid (row-1, col-1) == true)
            {
                 if (isMine (row-1, col-1, mineBoardObj.Board) == false)
                    playMinesweeperUtil(playerBoardObj.Board, mineBoardObj.Board, mineBoardObj.mines, row-1, col-1, movesLeft);
            }
 
             //----------- 7th Neighbour (South-East) ------------
 
            // Only process this cell if this is a valid one
            if (isValid (row+1, col+1) == true)
            {
                 if (isMine (row+1, col+1, mineBoardObj.Board) == false)
                    playMinesweeperUtil(playerBoardObj.Board, mineBoardObj.Board, mineBoardObj.mines, row+1, col+1, movesLeft);
            }
 
            //----------- 8th Neighbour (South-West) ------------
 
            // Only process this cell if this is a valid one
            if (isValid (row+1, col-1) == true)
            {
                if (isMine (row+1, col-1, mineBoardObj.Board) == false)
                    playMinesweeperUtil(playerBoardObj.Board, mineBoardObj.Board, mineBoardObj.mines, row+1, col-1, movesLeft);
            }
        }
 
        return (false);
    }
}


// A Function to play Minesweeper game
void playMinesweeper ()
{
    // Initially the game is not over
    bool gameOver = false;
   
    // Specify Moves Left
    int movesLeft = SIDE * SIDE - MINES, x, y;
    int mines[MAXMINES][2];

    // Initialize mineBoard and playerBoard to empty state
    mineBoardObj.reset();
    playerBoardObj.reset();

    // Place the Mines randomly on mineBoard
    mineBoardObj.placeMines(mines);  

    // You are in the game until you have not opened a mine
    // So keep playing
 
    int currentMoveIndex = 0;
    while (gameOver == false)
     {
        printf ("Current Status of Board : \n");
        playerBoardObj.printBoard();
        makeMove (&x, &y);
 
        // This is to guarantee that the first move is
        // always safe
        // If it is the first move of the game
        if (currentMoveIndex == 0)
        {
            // If the first move itself is a mine
            // then we remove the mine from that location
            if (isMine (x, y, mineBoardObj.Board) == true)
                replaceMine (x, y, mineBoardObj.Board);
        }
 
        currentMoveIndex ++;
 
        gameOver = playMinesweeperUtil (playerBoardObj.Board, mineBoardObj.Board, mineBoardObj.mines, x, y, &movesLeft);
 
        if ((gameOver == false) && (movesLeft == 0))
         {
            printf ("\nYou won !\n");
            gameOver = true;
            cin.get();
            cin.get();

         }
    }
    return;
}

// Driver Program to test above functions
int main()
{
    /* Choose a level between
    --> BEGINNER = 9 * 9 Cells and 10 Mines
    --> INTERMEDIATE = 16 * 16 Cells and 40 Mines
    --> ADVANCED = 24 * 24 Cells and 99 Mines
    */
   gameDifficulty gameDifficultyObj; 
   gameDifficultyObj.chooseDifficultyLevel(0.0);
 
    playMinesweeper ();
 
    return (0);
}
