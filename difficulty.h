using namespace std;

class gameDifficulty {
public:
    void chooseDifficultyLevel (double x)
    {
        /*
        --> BEGINNER = 9 * 9 Cells and 10 Mines
        --> INTERMEDIATE = 16 * 16 Cells and 40 Mines
        --> ADVANCED = 24 * 24 Cells and 99 Mines
        */
     
        int level;
     
        printf ("Enter the Difficulty Level\n");
        printf ("Press 0 for BEGINNER (9 * 9 Cells and 10 Mines)\n");
        printf ("Press 1 for INTERMEDIATE (16 * 16 Cells and 40 Mines)\n");
        printf ("Press 2 for ADVANCED (24 * 24 Cells and 99 Mines)\n");
     
        scanf ("%d", &level);
     
        if (level == BEGINNER)
        {
            SIDE = 9;
            MINES = 10;
        }
     
        if (level == INTERMEDIATE)
        {
            SIDE = 16;
            MINES = 40;
        }
     
        if (level == ADVANCED)
        {
            SIDE = 24;
            MINES = 99;
        }
     
        return;
    }

    void chooseDifficultyLevel (int level)
    {
        if (level == BEGINNER)
        {
            SIDE = 9;
            MINES = 10;
        }
     
        if (level == INTERMEDIATE)
        {
            SIDE = 16;
            MINES = 40;
        }
     
        if (level == ADVANCED)
        {
            SIDE = 24;
            MINES = 99;
        }
     
        return;
    }

};

