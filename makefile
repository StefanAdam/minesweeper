all: minesweeper_linux minesweeper_win debug

linux: minesweeper_linux

windows: minesweeper_win

minesweeper_linux: minesweeper_linux.o
	g++ -o minesweeper_linux minesweeper_linux.o -std=c++11

minesweeper_linux.o: 
	g++ -c minesweeper.cpp -o minesweeper_linux.o -std=c++11

minesweeper_win: minesweeper_win.o
	i686-w64-mingw32-c++ -o minesweeper_win.exe minesweeper_win.o -static-libgcc -static-libstdc++ -std=c++11

minesweeper_win.o: 
	i686-w64-mingw32-c++ -c minesweeper.cpp -o minesweeper_win.o -std=c++11

debug: 
	g++ -o minesweeper_debug -g minesweeper.cpp -std=c++11
