#!/bin/bash
## Enviroment Cleaning, purges .o, and compiled software.

if [ -f minesweeper_linux ]; then
	rm -v minesweeper_linux;
else
	echo "Compiled Linux Not Found";
fi

if [ -f minesweeper_win.o ]; then
	rm -v minesweeper_linux.o;
else
	echo "Linux Object File Not Found";
fi

if [ -f minesweeper_win.exe ]; then
	rm -v minesweeper_win.exe;
else
	echo "Compiled Windows Not Found";
fi

if [ -f minesweeper_win.o ]; then
	rm -v minesweeper_win.o;
else 
	echo "Windows Object File Not Found";
fi

if [ -f minesweeper_debug ]; then
	rm -v minesweeper_debug;
else 
	echo "Debug File Not Found";
fi
